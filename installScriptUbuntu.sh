#!/bin/bash

echo "Copyright <2017>  <Alex Wolf> <gitlab.com/flowalex>
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    The script will install programs that are useful for my daily workflow and 
    other customizations that are for my prefrence, anyone can add or remove 
    apps from the script, if there are any other important apps missing for 
    your daily needs."


echo "Do you accept the liscense terms [y/n]?"
read ans


if [ $ans = y -o $ans = Y -o $ans = yes -o $ans = Yes -o $ans = YES ]
then
mkdir installationfiles

cd installationfiles
sudo add-apt-repository ppa:noobslab/macbuntu -y

#Runs an initial update to  make sure that the OS is ready for the apps that are in the Ubuntu Repositories
sudo apt-get update && upgrade

#Adding gdebi for the apps that aren't in package managers so  that the apps that are downloaded from the web can be installed
sudo apt-get install gdebi -y

#I choose keepassx since I prefer it to keepass2 but both are good to use.
sudo apt-get install keepassx -y

#Optional since I don't use Wayland I use Plank dock but there is a good dash to dock in gnome shell extensions (if using gnome shell)
sudo apt-get install plank -y

#I use it for the the laser cutters I have access to also  nice alternative to Adobe Illustriator
sudo apt-get install inkscape -y

#Just a good program to  have installed
sudo apt-get install gnome-tweak-tool -y

#fun terminal app that gives information about the operating system that you are using and other details about the system.
sudo apt install neofetch -y

#A tool that lets you use the auto type for keepass
sudo apt-get install xdotool -y

#I prefer it to the default video player that comes installed
sudo apt-get install vlc -y

#qodem a terminal emulator, I use it to connect to a bbs
wget https://sourceforge.net/projects/qodem/files/qodem/1.0.0/qodem-x11_1.0.0-1_amd64.deb/download
sudo gdebi --non-interactive qodem-x11_1.0.0-1_amd64.deb


#syncthing app that syncronizes files across mutiple computers, still needs to be configured after installation for the folders that you want to  sync and to which devices they want to sync them to.
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
# Add the "stable" channel to your APT sources:
echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
# Update and install syncthing:
sudo apt-get update
sudo apt-get install syncthing -y


#Installs Lulzbot edition of Cura, for 3d printing
wget -qO - http://download.alephobjects.com/ao/aodeb/aokey.pub | sudo apt-key add -
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak && sudo sed -i '$a deb http://download.alephobjects.com/ao/aodeb stretch main' /etc/apt/sources.list
sudo apt-get update
sudo apt-get install cura -y


#Installs Google chrome (not open source version) for compatibility with some websites so that some of the websites I need still work.  Feel free to remove from script
wget https://dl.google.com/linux/linux_signing_key.pub
sudo apt-key add linux_signing_key.pub
sudo apt update
sudo apt install google-chrome-stable -y

#installs gimp (gnu image manipulation program) while I have darktable and inkscape I use gimp for photo editing more than the others.
sudo apt-get install gimp -y

#Installs darktable photo ediitor
sudo add-apt-repository ppa:pmjdebruijn/darktable-release  -y
sudo apt-get update
sudo apt-get install darktable -y

#Installs synergy from symless, but it requires it to have been purchased, from symless and get a product key, they are releasing version 2 soon and it will be easer to configur their clients
#sudo gdebi --non-interactive synergy-v1.8.8-stable-25a8cb2-Linux-x86_64.deb
wget https://symless.com/synergy/download/direct?platform=debian&architecture=x64
sudo gdebi --non-interactive synergy-v1.8.8-stable-25a8cb2-Linux-x86_64.deb




#Installs gitkraken
wget https://release.gitkraken.com/linux/gitkraken-amd64.deb
sudo gdebi --non-interactive gitkraken-amd64.deb

wget http://mirrors.kernel.org/ubuntu/pool/main/libp/libpng/libpng12-0_1.2.54-1ubuntu1_amd64.deb
sudo gdebi --non-interactive libpng12-0_1.2.54-1ubuntu1_amd64.deb

#QEMU a generic and open source machine emulator and virtualizer.
sudo apt-get install qemu -y

# Installs spotify as a way to stream music, it isn't open source, but I think it is fine for my daily use
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys BBEBDCB318AD50EC6865090613B00F1FD2C19886 0DF731E45CE24F27EEEB1450EFDC8610341D9410
echo deb http://repository.spotify.com stable non-free | sudo tee /etc/apt/sources.list.d/spotify.list
sudo apt-get update
sudo apt-get install spotify-client -y

#handbrake,  a tool for converting video from nearly any format to a selection of modern, widely supported codecs
sudo add-apt-repository ppa:stebbins/handbrake-releases
sudo apt-get update
sudo apt-get install handbrake -y

#shuttera screenshot tool
sudo add-apt-repository ppa:shutter/ppa
sudo apt-get update && sudo apt-get install shutter -y

#some fun terminal apps
#steam locomotive
sudo apt-get install sl -y

#cowsay
sudo apt-get install cowsay -y

#Just an app that adds an equalizer to linux so that when listening to music it can be tuned to the audio device that is being used.
sudo gdebi --non-interactive pulseeffects_1.312entornosgnulinuzesty-1ubuntu1_amd64.deb


#Adds the web browser Vivaldi, that I use as my default, I think firefox is a good browser but I prefer the features that it has over chrome and firefox
sudo sh -c "echo 'deb http://repo.vivaldi.com/stable/deb/ stable main' >> /etc/apt/sources.list.d/vivaldi.list"
wget -O vr.key http://repo.vivaldi.com/stable/linux_signing_key.pub;sudo apt-key add - < vr.key;rm vr.key
sudo apt-get update
sudo apt-get install vivaldi-stable -y

#installs clipgrab a piece of software that downloads videos from websites.
sudo add-apt-repository ppa:noobslab/apps  -y
sudo apt-get update
sudo apt-get install clipgrab -y

#Syncthing a data transfer client that transfers computer to computer with no server that isn't owned by some company.  I use the stable channel, but there is a canidate channel as well.
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
sudo apt-get update
sudo apt-get install syncthing -y


#installs gnome-builder, I think that it is a great IDE for linux, OS, POP!_OS is based off of Ubuntu with the gnome desktop enviroment.
sudo apt-get install gnome-builder -y

#installs stacer a system monitor app
wget https://github.com/oguzhaninan/Stacer/releases/download/v1.0.8/stacer_1.0.8_amd64.deb
sudo dpkg -i noninteractive  stacer*.deb

sudo apt-get install qemu-kvm qemu virt-manager virt-viewer libvirt-bin -y


#installs thunderbird, OPTIONAL Disable if you want to use the default one
sudo add-apt-repository ppa:mozillateam/thunderbird-stable -y
sudo apt-get update
sudo apt-get install thunderbird -y

#checks for the version of Ubuntu that is being run, for programs  that are version specific
if [[ `lsb_release -rs` == "17.10" ]] #I will keep it added to the script until 18.04 comes out and it will be replaced, also  since 18.04 is a LTS release it will be kept in the script until out of support, and I will keep adding new versions of Ubuntu, execpt the non LTS will be replaced every time a new version comes out.

wget -O mac-fonts.zip http://drive.noobslab.com/data/Mac/macfonts.zip
sudo unzip mac-fonts.zip -d /usr/share/fonts; rm mac-fonts.zip
sudo fc-cache -f -v


then
#installs the client for Megasync, a cloud provider that gives 50 GB of free storage that is encrypted.
wget https://mega.nz/linux/MEGAsync/xUbuntu_17.10/amd64/megasync-xUbuntu_17.10_amd64.deb
sudo gdebi --non-interactive megasync-xUbuntu_17.10_amd64.deb
sudo apt-get install macbuntu-os-plank-theme-v9 -y
#installs the application cool retro term, while not necessary,  it is a fun terminal emulator to have, quoting Bryan Lunduke "Who doesn't want thei terminal to look like an old IBM computer
sudo add-apt-repository ppa:noobslab/apps -y
echo "deb http://archive.ubuntu.com/ubuntu/ xenial main universe" | sudo tee /etc/apt/sources.list.d/depd-CRT.list
sudo apt-get update
sudo apt-get install cool-retro-term -y
sudo apt-get install qml-module-qt-labs-settings qml-module-qt-labs-folderlistmodel -y
sudo apt-get -f install;sudo apt-get install cool-retro-term -y
sudo dpkg -i noninteractive  --force-overwrite /var/cache/apt/archives/cool-retro-term*.deb;sudo rm /etc/apt/sources.list.d/depd-CRT.list;
sudo apt-get update

fi

elif [[ `lsb_release -rs` == "16.04" ]] # 16.04 since it is a LTS release I will keep it added to the script until it is no longer supported.
wget https://mega.nz/linux/MEGAsync/xUbuntu_16.04/amd64/megasync-xUbuntu_16.04_amd64.deb
sudo gdebi --non-interactive megasync-xUbuntu_16.04_amd64.deb
sudo apt-get install macbuntu-os-plank-theme-lts-v7 -y
sudo add-apt-repository ppa:noobslab/apps -y
sudo apt-get update
sudo apt-get install cool-retro-term -y

chmod +x removeexecutable.sh
./removeexecutable.sh
fi




fi


if [ $ans = n -o $ans = N -o $ans = no -o $ans = No -o $ans = NO ]
then
echo "Exit"
chmod +x removeexecutable
./removeexecutable.sh
exit
fi


